"--------
" PLUGINS
"--------

call plug#begin('~/.local/share/nvim/plugged')
" Airline Bar
        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'
" Colour Scheme
        Plug 'chriskempson/base16-vim'
" File Browser
        Plug 'preservim/nerdtree'
" Git wrapper
        Plug 'tpope/vim-fugitive'
" Languages & File Types
" Language Server
        Plug 'autozimu/LanguageClient-neovim', {
        \ 'branch': 'next',
        \ 'do': 'bash install.sh',
        \ }
" Go
        Plug 'fatih/vim-go', {'for': 'go',
        \'do': ':GoUpdateBinaries'}
" Autocomplete
        Plug 'ncm2/ncm2'
        Plug 'roxma/nvim-yarp'
        "Directory autocomplete
        Plug 'ncm2/ncm2-path'
call plug#end()

"-------
" CONFIG
"-------

        set number
        set background = "dark"
        syntax enable

" Tab = 4 spaces, tabs spaces for compat
        set tabstop=4
        set softtabstop=4
        set expandtab

" Highlight line/matching quotes
        set cursorline
        set showmatch

" Autocomplete brackets/quotes
        inoremap " ""<left>
        inoremap ' ''<left>
        inoremap ` ``<left>
        inoremap ( ()<left>
        inoremap [ []<left>
        inoremap { {}<left>
        inoremap < <><left>

" CTRL+V to Cancel autocomplete above
        inoremap {<CR> {<CR>}<ESC>O
        inoremap {;<CR> {<CR>};<ESC>O

" Enable spellcheck on chosen file types
        autocmd BufRead,BufNewFile *.txt,*.md,*.tex 
        \ setlocal spell spelllang=en_au

" Theming
        let g:airline_theme='murmur'
        colorscheme base16-material-darker
        set termguicolors

" Ctrl+N to open file browser
        map <C-n> :NERDTreeToggle<CR>

" Language Servers
        let g:LanguageClient_serverCommands = {
        \ 'go': ['~/go/bin/gopls']
        \}  

        let g:python3_host_prog='python3'

        autocmd BufWritePre *.go :call LanguageClient#textDocument_formatting_sync()

" Enable ncm2 (Autocomplete)
        autocmd BufEnter * call ncm2#enable_for_buffer()
        set completeopt=noinsert,menuone,noselect
